#include "mainWindow.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <armadillo>


using namespace std;
using namespace arma;

mainWindow::mainWindow(int w, int h ,const char * title):keyEventWindow(w, h, title)
{
	begin();
		menu =new Fl_Menu_Bar(0, 0, this->w(), 25);
		menu->add("&Help/&Description", 0, help);
        com1 =new  FileChooserCom(50,50,500,40,"Open 2D points file");
        com2 =new  FileChooserCom(50,150,500,40,"Open 3D points file");
        com3 =new  FileChooserCom(50,250,500,40,"Open RANSAC config file");
        runalg = new Fl_Button(50,350,500,40,"Run");
        runalg -> callback(algrun,(void*)this);
	end();
	Fl::focus(this);
	cache =0;
	this->show();


}

mainWindow::~mainWindow()
{

}

void mainWindow::help(Fl_Widget* widget, void * data){
    std::string msg("please read two points files and press run to get result in command line");
    std::cout << msg << std::endl;
    fl_alert(msg.c_str());
}

void mainWindow::algrun(Fl_Widget *w, void *data){


    ((mainWindow*)data)->algrun2();
}


void MyCalibration(mat &k,mat &t,mat &r,mat &big){
    mat V,U;
    vec s;
    svd( U, s, V,big);
    vec eigval = V.col(11);
    vec a1(3); a1 << eigval(0) <<eigval(1) << eigval(2);
    vec a2(3); a2 << eigval(4) <<eigval(5) << eigval(6);
    vec a3(3); a3 << eigval(8) <<eigval(9) << eigval(10);
    vec b(3) ; b  << eigval(3) <<eigval(7) << eigval(11);
    double p = 1/sqrt(dot(a3,a3));
    double u0 = p*p*dot(a1,a3);
    double v0 = p*p*dot(a2,a3);
    double alphav = sqrt(p*p*dot(a2,a2)-v0*v0);
    double s0 = p*p*p*p/alphav*dot(cross(a1,a3),cross(a2,a3));
    //s0=0;// usually s0 = 0
    double alphau = sqrt(p*p*dot(a1,a1)-s0*s0-v0*v0);
    k<< alphau << s0 << u0<<endr << 0 << alphav << v0 <<endr << 0 << 0 << 1<<endr;

    double epu;
    if(b(2)>0){
        epu = 1;
    }
    else if(b(2)<0){
        epu = -1;
    }
    else{
        epu =0;
    }
    t = epu*p*inv(k)*b;
    vec r3 = epu*p*a3;
    vec r1 = p*p/alphav*cross(a2,a3);
    vec r2 = cross(r3,r1);
    r << r1(0) << r1(1) << r1(2) <<endr << r2(0) << r2(1) << r2(2) <<endr <<r3(0) << r3(1) << r3(2) <<endr;
}


void MyCalibration0(mat &k,mat &t,mat &r,vector<mat> &p4, vector<mat> &p2){
    mat superbig;
    mat z4 =  zeros<mat>(1,4);
    for(int i = 0; i<p4.size();i++){
        mat tempp = p2[i];
        double x1 = tempp(0);
        double y1 = tempp(1);

        mat m112;
        m112 = join_horiz(m112,p4[i]);
        m112 = join_horiz(m112,z4);
        m112 = join_horiz(m112, (x1)*(-1)*p4[i]);
        mat m112d;
        m112d = join_horiz(m112d,z4);
        m112d = join_horiz(m112d,p4[i]);
        m112d = join_horiz(m112d,(y1)*(-1)*p4[i]);
        superbig = join_vert(superbig,m112);
        superbig = join_vert(superbig,m112d);

    }
    MyCalibration(k,t,r,superbig);
}
//int myrandom (int i) { return std::rand()%i;}
void randompoints(vector<mat> &p3d, vector<mat> &p2d,vector<mat> &op3d, vector<mat> &op2d,vector<int> &indexchoose,
                  vector<int> &indexother, int number){
    vector<int> indexvector;
    for (int i=0; i<p3d.size(); ++i) indexvector.push_back(i);

    std::random_shuffle ( indexvector.begin(), indexvector.end()  );

    op3d.clear();
    op2d.clear();
    indexchoose.clear();
    indexother.clear();
    int i = 0;
    for( ;i<indexvector.size();i++){
        if(i< number){
        op3d.push_back(p3d[indexvector[i]]);
        op2d.push_back(p2d[indexvector[i]]);
        indexchoose.push_back(indexvector[i]);
        }
        else{
            indexother.push_back(indexvector[i]);
        }
    }
}

double checkerror(mat &k,mat &t,mat &r, mat &d3, mat &d2){
    vec m3; m3<<d3[0]<<d3[1]<<d3[2]<<1;
    mat A;
    A << r(0,0) <<r(0,1) <<r(0,2) <<t(0,0)<<endr
      << r(1,0) <<r(1,1) <<r(1,2) <<t(1,0)<<endr
      << r(2,0) <<r(2,1) <<r(2,2) <<t(2,0)<<endr;
    mat M = k*A;
    vec s = M*m3;
    double x1 = s(0)/s(2);
    double y1 = s(1)/s(2);

    double error = pow((d2(0)-x1),2)+pow((d2(1)-y1),2);
    //cout << error <<endl;
    return error;
}


void MyRANSAC(vector<mat> &p3d, vector<mat> &p2d, int itrn, int samplep, double errtheshold){
    std::srand ( unsigned ( std::time(0) ) );
    vector<mat> randp3d;
    vector<mat> randp2d;
    vector<int> indexchoose;
    vector<int> indexother;

    mat bestk,bestt,bestr;
    double best_error = 100000000000000;
    vector<int> best_set;
    vector<int> p_set;
    for(int iteration = 0 ;iteration<itrn ; iteration++){
        p_set.clear();
        randompoints(p3d,p2d,randp3d,randp2d,indexchoose,indexother,samplep);
        p_set = indexchoose;
        mat k,t,r;
        MyCalibration0(k,t,r,randp3d,randp2d);
        double totalerr =0;
        for(int i =0;i<indexchoose.size();i++){
            double err = checkerror(k,t,r, p3d[indexchoose[i]],p2d[indexchoose[i]]);
            totalerr+=err;
        }
        for(int i =0;i<indexother.size();i++){
            double err = checkerror(k,t,r, p3d[indexother[i]],p2d[indexother[i]]);
            if(err < errtheshold){
                totalerr+=err;
                p_set.push_back(indexother[i]);
                //cout << p_set.size()<<endl;
            }

        }
        double avgerr = totalerr/p_set.size();
            if(avgerr < best_error){
                    best_error = avgerr;
                    best_set.clear();
                    best_set = p_set;
                    bestk = k;
                    bestt = t;
                    bestr = r;
            }

    }

    cout << "best K" << bestk <<endl;
    cout << "best T" << bestt <<endl;
    cout << "best R" << bestr <<endl;
    cout << "average error: " <<best_error<<endl;
    cout << "consensus set  size :" << best_set.size() <<endl;
}


void mainWindow::algrun2(){

    vector<mat> p2d;
    vector<mat> p3d;
    ifstream myfile1 (this->com1->getvalue().c_str());
    ifstream myfile2 (this->com2->getvalue().c_str());
    ifstream myfile3 (this->com3->getvalue().c_str());
    int itrn; //iteration number
    int minimamSamplePoints;
    double errorThreshold;

    if(myfile3.is_open()){
        myfile3 >> itrn;
        myfile3 >> minimamSamplePoints;
        myfile3 >> errorThreshold;
    }
    else{
        cout << "config file didn't found"<<endl;
        return;
    }

    if (myfile1.is_open()&&myfile2.is_open()){
        int num1;
        myfile1 >> num1;
        int num2;
        myfile2 >> num2;
        mat z4 =  zeros<mat>(1,4);
        for(int i = 0 ;i<num1; i++){
            double x1,y1,x,y,z;
            myfile1 >> x1;
            myfile1 >> y1;
            myfile2 >> x;
            myfile2 >> y;
            myfile2 >> z;

            mat p; p << x << y <<z <<1;
            mat p2; p2<< x1<<y1;
            p3d.push_back(p);
            p2d.push_back(p2);
        }
        //mat k,t,r;
        //MyCalibration0(k,t,r,p3d,p2d);
        MyRANSAC(p3d,p2d,itrn,minimamSamplePoints,errorThreshold);

        myfile1.close();
        myfile2.close();
    }
}
