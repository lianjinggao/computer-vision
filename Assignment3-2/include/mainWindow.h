#pragma once
#include "keyEventWindow.h"
#include "wincommon.h"
#include "mywidgets.hpp"

class mainWindow :
	public keyEventWindow
{
public:
	mainWindow(int, int, const char *);
	~mainWindow();
	int cache;
private:
	Fl_Menu_Bar * menu;

	FileChooserCom * com1;
	FileChooserCom * com2;
	FileChooserCom * com3;
    Fl_Button * runalg;

	static void help(Fl_Widget* widget, void * data);
	static void algrun(Fl_Widget *w, void *data);
    void algrun2();
};

