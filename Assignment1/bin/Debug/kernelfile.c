kernel void test(global float*A, global float*B, global float* C,global int * width){

	size_t gx = get_global_id(0);
	uint dim = get_work_dim();
	size_t gy = get_global_id(1);
	size_t gs = get_global_size(0);	

	C[gx+gy*gs] = (float)gs ;
}

kernel void channelB(global float *A){
	int i= get_global_id(0);
	if(i%3==0){
		A[i]=A[i];
	}
	else{
		A[i]=0;
	}
}

kernel void channelG(global float *A){
	int i= get_global_id(0);
	if(i%3==1){
		A[i]=A[i];
	}
	else{
		A[i]=0;
	}
}

kernel void channelR(global float *A){
	int i= get_global_id(0);
	if(i%3==2){
		A[i]=A[i];
	}
	else{
		A[i]=0;
	}
}

// 3 channel to 1 channel grayscale
kernel void grayscaleBGR(global uchar *src, global uchar *dst){
	int i= get_global_id(0);
	dst[i] = src[3*i]*0.0722 + 0.7152*src[3*i+1] + src[3*i+2]*0.2126;
	
}

kernel void grayscaleRGB(global uchar *src, global uchar *dst){
	int i= get_global_id(0);
	dst[i] = src[3*i]*0.2126 + 0.7152*src[3*i+1] + src[3*i+2]*0.0722;
}

// filtersize is radius
kernel void convolutefloat1(global float * input, global float * output, constant float *filter, global int * filtersize){
	int fIndex = 0;
	int gx = get_global_id(0);
	int gs = get_global_size(0);
	int gy = get_global_id(1);

	int half_fw = filtersize[0];
	int current = gx+gy*gs;

	float sumR = 0.0f;

	for (int r = -half_fw; r <= half_fw; r++){
		int curRow = gx+(gy+r)*gs;
		for (int c = -half_fw; c <= half_fw; c++, fIndex += 1){
			sumR += input[curRow + c] * filter[fIndex];
		}
	}
	output[current] = sumR;
}

kernel void convoluteImage(read_only image2d_t srcImg, write_only image2d_t dstImg, sampler_t sampler, global float *filter, int filterRadius){
	int k1 = get_global_id(0) - filterRadius;
	int k2 = get_global_id(1) - filterRadius;
	int k3 = get_global_id(0) + filterRadius;
	int k4 = get_global_id(1) + filterRadius;


	int2 startImageCoord = (int2) (k1, k2);
	int2 endImageCoord 	 = (int2) (k3, k4);
	int2 outImageCoord   = (int2) (get_global_id(0)     , get_global_id(1)    );
	int filterStep = 0;
	float4 outColor = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	
	for(int y = startImageCoord.y; y <= endImageCoord.y; y++){
			for(int x= startImageCoord.x; x <= endImageCoord.x; x++ , filterStep++){
				outColor +=  (read_imagef(srcImg, sampler, (int2)(x, y)) * (filter[filterStep]));
			}
	}
	write_imagef(dstImg, outImageCoord, outColor);
	
}

kernel void gaussian_filter(read_only image2d_t srcImg, write_only image2d_t dstImg, sampler_t sampler, int width, int height){
// Gaussian Kernel is:
// 1 2 1
// 2 4 2
// 1 2 1
	float kernelWeights[9]={1.0f, 2.0f, 1.0f,
							2.0f, 4.0f, 2.0f,
							1.0f, 2.0f, 1.0f};
	int2 startImageCoord = (int2) (get_global_id(0) - 1, get_global_id(1) - 1);
	int2 endImageCoord 	 = (int2) (get_global_id(0) + 1, get_global_id(1) + 1);
	int2 outImageCoord   = (int2) (get_global_id(0), get_global_id(1));
	//if (outImageCoord.x < width && outImageCoord.y < height){
		int weight = 0;
		float4 outColor = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
		for(int y = startImageCoord.y; y <= endImageCoord.y; y++){
			for(int x= startImageCoord.x; x <= endImageCoord.x; x++,weight++){
				outColor += 
				(read_imagef(srcImg, sampler, (int2)(x, y)) * 
				(kernelWeights[weight] / 16.0f));
			}
		}
	// Write the output value to image
	write_imagef(dstImg, outImageCoord, outColor);
	//}
}