#include "loopWindow.h"
using namespace std;
loopWindow::loopWindow(int w, int h ,const char * title):keyEventWindow(w, h, title)
{
    thread_exit_flag = 0;
    int ret;
    ret = pthread_create(&thread, NULL, Thread_CB, this);
}

loopWindow::~loopWindow()
{
    thread_exit_flag = 1;
    pthread_join(thread,NULL);
}
void loopWindow::setFreshRate(int rate){
    freshRate = rate;
}

int loopWindow::getFreshRate(){
    return freshRate;
}

void loopWindow::Timer_CB(void* userdata){
    loopWindow *o = (loopWindow*)userdata;
    o->redraw();
    if(o->freshRate!=0){
        Fl::repeat_timeout(1.0/o->freshRate, Timer_CB, userdata);
    }
}

void* loopWindow::Thread_CB(void* userdata){
    loopWindow *o = (loopWindow*)userdata;
    for(int i =0 ;i==o->thread_exit_flag;){
        cout << "loopWindow running..." <<endl;
    }
    cout << "loopWindow exiting..." <<endl;
    return NULL;
}

void loopWindow::draw(){
    Fl_Window::draw();
}
