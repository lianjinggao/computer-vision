#include "fumeAlg.h"
#include <cmath>
using namespace std;
fumeAlg::fumeAlg()
{
   //fumeCL fuCL= fumeCL();
   //bundle =  &fuCL.getBundle(bestGPU);
    pi = 3.14159;
}

fumeAlg::~fumeAlg()
{
    //dtor
}

fumeFilter fumeAlg::gaussianFilter(int radius,float sigma){
    float * data = new float[(radius*2+1)*(radius*2+1)];
    int index=0;
    float sum = 0;
    for(int w = 1;w<=radius*2+1 ;w++){
        for(int h =1;h<=radius*2+1;h++,index++){
            float power = pow(exp(1),-((pow(w-radius-1,2)+pow(h-radius-1,2))/2*pow(sigma,2)));
            data[index] =power;
            sum += power;
        }
    }

    for(int i=0;i<index;i++ ){
        data[i] = data[i]/ sum;
    }
    /*
    sum = 0;
    for(int i=0;i<index;i++ ){
        sum += data[i];
    }
    std::cout << "sum :" << sum << std::endl;*/
    return fumeFilter(radius,2*radius+1,2*radius+1,data,sizeof(float));

}

fumeFilter fumeAlg::simpleFilter(int radius){
    float * data = new float[(radius*2+1)*(radius*2+1)];
    int index=0;
    float sum = (radius*2+1)*(radius*2+1);

    for(int w = 1;w<=radius*2+1 ;w++){
        for(int h =1;h<=radius*2+1;h++,index++){
            data[index] += 1/sum;
        }
    }
    return fumeFilter(radius,2*radius+1,2*radius+1,data,sizeof(float));
}
fumeFilter::~fumeFilter(){
    delete [] data;
}
