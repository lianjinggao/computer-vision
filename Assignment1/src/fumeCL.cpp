#include "fumeCL.h"

using namespace std;
fumeCL::fumeCL()
{
	int err = cl::Platform::get(&platforms);
	if (err != CL_SUCCESS || platforms.size() == 0) {
		std::cerr << "Could not get available platforms." << std::endl;
	}
	platFormNumber = platforms.size();

	for (size_t i = 0; i < platFormNumber; i++){
		cl::Platform platform = platforms[i];
		std::string name, vendor, profile, version;
		platform.getInfo(CL_PLATFORM_NAME, &name);
		platform.getInfo(CL_PLATFORM_VENDOR, &vendor);
		platform.getInfo(CL_PLATFORM_PROFILE, &profile);
		platform.getInfo(CL_PLATFORM_VERSION, &version);

		std::cout << "Platform " << name << " (" << i << ") vendor = " << vendor
			<< ", profile = " << profile << ", version = " << version
			<< std::endl << std::endl;
		std::cout << "=== GPUs ===" << std::endl;
		dumpPlatform(CL_DEVICE_TYPE_GPU, platform);

		std::cout << "=== CPUs ===" << std::endl;
		dumpPlatform(CL_DEVICE_TYPE_CPU, platform);
        /*
		std::cout << "=== Accelerators ===" << std::endl;
		dumpPlatform(CL_DEVICE_TYPE_ACCELERATOR, platform);

		std::cout << "=== Default device ===" << std::endl;
		dumpPlatform(CL_DEVICE_TYPE_DEFAULT, platform, true);
		*/
	}
}


fumeCLBundle fumeCL::getBundle(bundleType T){
    if(T == bestGPU){
        size_t unitNum = 0;
        cl::Context resContext;
        std::vector<cl::Device> resDevices;
        cl::Device resDevice;
        for(size_t i = 0 ; i< platFormNumber; i++){
            cl::Platform platform = platforms[i];
            cl_context_properties properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)(platform)(), 0 };
            cl::Context context = cl::Context(CL_DEVICE_TYPE_GPU, properties);
            std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
            for(size_t k = 0; k<devices.size(); k++){
                cl_uint maxCompUnits;
                devices[k].getInfo<cl_uint>(CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxCompUnits);
                if(unitNum < maxCompUnits){
                    unitNum = maxCompUnits;
                    resContext = context;
                    resDevices = devices;
                    resDevice  = devices[k];
                }
            }
        }
        string name;
        resDevice.getInfo<string>(CL_DEVICE_NAME, &name);
        cout << "picked device device name: " <<name << endl;
        return fumeCLBundle(resContext, resDevice);
    }
    else if( T == bestCPU){



    }
    return fumeCLBundle();
}

fumeCLBundle* fumeCL::getBundleInheap(bundleType T){
    fumeCLBundle * res;
    if(T == bestGPU){
        size_t unitNum = 0;
        cl::Context resContext;
        std::vector<cl::Device> resDevices;
        cl::Device resDevice;
        for(size_t i = 0 ; i< platFormNumber; i++){
            cl::Platform platform = platforms[i];
            cl_context_properties properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)(platform)(), 0 };
            cl::Context context = cl::Context(CL_DEVICE_TYPE_GPU, properties);
            std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
            for(size_t k = 0; k<devices.size(); k++){
                cl_uint maxCompUnits;
                devices[k].getInfo<cl_uint>(CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxCompUnits);
                if(unitNum < maxCompUnits){
                    unitNum = maxCompUnits;
                    resContext = context;
                    resDevices = devices;
                    resDevice  = devices[k];
                }
            }
        }
        string name;
        resDevice.getInfo<string>(CL_DEVICE_NAME, &name);
        cout << "picked device device name: " <<name << endl;
        res = new fumeCLBundle(resContext, resDevice);
        return res;
    }
    else if( T == bestCPU){



    }
    
    return res;
}


fumeCL::~fumeCL()
{

}

void fumeCL::dumpPlatform(int type, cl::Platform platform, bool brief) {
	cl_context_properties properties[] =
	{ CL_CONTEXT_PLATFORM, (cl_context_properties)(platform)(), 0 };

	cl::Context context = cl::Context(type, properties);
	std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
	if (devices.size() == 0) {
		std::cerr << "Could not find any devices of this type." << std::endl;
		return;
	}

	fumeCLBundle bundle(context, devices);
	std::cout << "push" << std::endl;
	pool.push_back(bundle);

	for (size_t i = 0; i < devices.size(); i++) {
		if (!brief) {
			std:: cout << "Device " << i << ":" <<	std:: endl;
		}
		dumpDevice(devices[i], brief);
		std::cout << std::endl;
	}
}

void fumeCL::dumpDevice(cl::Device device, bool brief){
	std::string name, vendor, profile, version, extensions;
	device.getInfo<std::string>(CL_DEVICE_NAME, &name);
	device.getInfo<std::string>(CL_DEVICE_VENDOR, &vendor);
	device.getInfo<std::string>(CL_DEVICE_PROFILE, &profile);
	device.getInfo<std::string>(CL_DEVICE_VERSION, &version);
	device.getInfo<std::string>(CL_DEVICE_EXTENSIONS, &extensions);

	cl_bool available, compAvailable, littleEndian, errCorrect, imgSupport;
	device.getInfo<cl_bool>(CL_DEVICE_AVAILABLE, &available);
	device.getInfo<cl_bool>(CL_DEVICE_COMPILER_AVAILABLE, &compAvailable);
	device.getInfo<cl_bool>(CL_DEVICE_ENDIAN_LITTLE, &littleEndian);
	device.getInfo<cl_bool>(CL_DEVICE_ERROR_CORRECTION_SUPPORT, &errCorrect);
	device.getInfo<cl_bool>(CL_DEVICE_IMAGE_SUPPORT, &imgSupport);

	cl_uint addrBits, maxClock, maxCompUnits, maxConstArgs, maxParmSize,
		maxSamplers, maxWorkGrpSize, maxWorkItmDim, globalMemCacheline;
	device.getInfo<cl_uint>(CL_DEVICE_ADDRESS_BITS, &addrBits);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_CLOCK_FREQUENCY, &maxClock);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_COMPUTE_UNITS, &maxCompUnits);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_CONSTANT_ARGS, &maxConstArgs);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_PARAMETER_SIZE, &maxParmSize);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_SAMPLERS, &maxSamplers);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxWorkGrpSize);
	device.getInfo<cl_uint>(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &maxWorkItmDim);
	device.getInfo<cl_uint>(CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, &globalMemCacheline);

	cl_ulong maxConstBufSize, maxMemAllocSize, globalMemCacheSize, globalMemSize,
		localMemSize;
	device.getInfo<cl_ulong>(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, &maxConstBufSize);
	device.getInfo<cl_ulong>(CL_DEVICE_MAX_MEM_ALLOC_SIZE, &maxMemAllocSize);
	device.getInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, &globalMemCacheSize);
	device.getInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_SIZE, &globalMemSize);
	device.getInfo<cl_ulong>(CL_DEVICE_LOCAL_MEM_SIZE, &localMemSize);

	std::vector<size_t> workItemSizes;
	device.getInfo<std::vector<size_t> >(CL_DEVICE_MAX_WORK_ITEM_SIZES, &workItemSizes);

	cl_device_fp_config dfpConfig, sfpConfig;
	device.getInfo<cl_device_fp_config>(CL_DEVICE_DOUBLE_FP_CONFIG, &dfpConfig);
	device.getInfo<cl_device_fp_config>(CL_DEVICE_SINGLE_FP_CONFIG, &sfpConfig);

	cl_device_exec_capabilities execCap;
	device.getInfo<cl_device_exec_capabilities>(CL_DEVICE_EXECUTION_CAPABILITIES,
		&execCap);

	cl_device_mem_cache_type memCacheType;
	device.getInfo<cl_device_mem_cache_type>(CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
		&memCacheType);

	cl_device_local_mem_type localMemType;
	device.getInfo<cl_device_local_mem_type>(CL_DEVICE_LOCAL_MEM_TYPE, &localMemType);

	cl_command_queue_properties cmdQueueProps;
	device.getInfo<cl_command_queue_properties>(CL_DEVICE_QUEUE_PROPERTIES,
		&cmdQueueProps);

	std::cout << "Name: " << name << std::endl
		<< "Vendor: " << vendor << std::endl;

	if (brief) return;

	std::cout << "Profile: " << profile << std::endl
		<< "Version: " << version << std::endl
		<< "Extensions: " << extensions << std::endl
		<< "Available: " << (available ? "YES" : "NO") << std::endl
		<< "Compiler available: " << (compAvailable ? "YES" : "NO") << std::endl
		<< "Endianess: " << (littleEndian ? "Little" : "Big") << std::endl
		<< "Error correction support: " << (errCorrect ? "YES" : "NO") << std::endl
		<< "Image support: " << (imgSupport ? "YES" : "NO") << std::endl
		<< "Address space: " << addrBits << " bits" << std::endl
		<< "Global memory size: " << globalMemSize << " bytes" << std::endl
		<< "Global memory cache: ";

	if (memCacheType == CL_NONE) {
		std::cout << "none" << std:: endl;
	}
	else if (memCacheType == CL_READ_ONLY_CACHE) {
		std::cout << "read-only" << std:: endl;
	}
	else if (memCacheType == CL_READ_WRITE_CACHE) {
		std::cout << "read-write" << std::endl;
	}

	if (memCacheType != CL_NONE) {
		std::cout << "   Size: " << globalMemCacheSize << " bytes" << std::endl
			<< "   Cache line: " << globalMemCacheline << " bytes" << std::endl;
	}

	std::cout << "Local memory: " << std::endl
		<< "   Type: " << (localMemType == CL_LOCAL ? "local" : "global") << std::endl
		<< "   Size: " << localMemSize << " bytes" << std::endl
		<< "Max clock frequency: " << maxClock << " MHz" << std::endl
		<< "Max compute units: " << maxCompUnits << std::endl
		<< "Max __constant arguments: " << maxConstArgs << std::endl
		<< "Max buffer size: " << maxConstBufSize << " KB" << std::endl
		<< "Max memory allocation: " << maxMemAllocSize << " bytes" << std::endl
		<< "Max kernel arguments: " << maxParmSize << std::endl
		<< "Max samplers pr. kernel: " << maxSamplers << std::endl
		<< "Max number of work-items in a work-group: " << maxWorkGrpSize << std::endl
		<< "Max dimensions for global and local work-items: " << maxWorkItmDim << std::endl
		<< "Max work-items pr. dimension: (";

	size_t tot = workItemSizes.size();
	for (size_t i = 0; i < tot; i++) {
		std::cout << workItemSizes[i];
		if (i < tot - 1) std::cout << ", ";
	}

	std::cout << ")" << std::endl
		<< "Double precision floating-point capabilities:" << std::endl;
	dumpFPConfig(dfpConfig);
	std::cout << "Single precision floating-point capabilities:" << std::endl;
	dumpFPConfig(sfpConfig);

	std::cout << "Execution capabilities:" << std::endl;
	if (execCap & CL_EXEC_KERNEL) {
		std::cout << "   Can execute OpenCL kernels." << std::endl;
	}
	if (execCap & CL_EXEC_NATIVE_KERNEL) {
		std::cout << "   Can execute native kernels." << std::endl;
	}

	std::cout << "Command queue properties:" << std::endl;
	if (cmdQueueProps & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE) {
		std::cout << "   Out-of-order execution enabled." << std::endl;
	}
	if (cmdQueueProps & CL_QUEUE_PROFILING_ENABLE) {
		std::cout << "   Profiling enabled." << std::endl;
	}
}

void fumeCL::dumpFPConfig(cl_device_fp_config bitfield){
	if (bitfield & CL_FP_DENORM) {
		std::cout << "   Denorms are supported." << std::endl;
	}
	if (bitfield & CL_FP_INF_NAN) {
		std::cout << "   INF and NaNs are supported." << std::endl;
	}
	if (bitfield & CL_FP_ROUND_TO_NEAREST) {
		std::cout << "   Round to nearest even rounding mode supported." << std::endl;
	}
	if (bitfield & CL_FP_ROUND_TO_ZERO) {
		std::cout << "   Round to zero mode supported." << std::endl;
	}
	if (bitfield & CL_FP_ROUND_TO_INF) {
		std::cout << "   Round to +ve and -ve infinity rounding mode supported." << std::endl;
	}
	if (bitfield & CL_FP_FMA) {
		std::cout << "   IEEE754-2008 fused multiply-add is supported." << std::endl;
	}
}
