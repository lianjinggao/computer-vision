#include "Fl_cvDisplay.h"
#include "mainWindow.h"
#include "featureDetect.h"

using namespace std;

void Fl_cvDisplay::saveimg(){
    imwrite(" output.jpg ",backup);
}

void Fl_cvDisplay::Timer_CB(void* userdata){
    Fl_cvDisplay *o = (Fl_cvDisplay*)userdata;
    o->redraw();
    Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
}


Fl_cvDisplay::Fl_cvDisplay(int x, int y, int w, int h,const char* l) :Fl_Box(x,y,w,h,l)
{
    modeG =1;
    char cwd[1024];
	_getcwd(cwd, sizeof(cwd));
	string current(cwd);
	currentpath = current;
	currentpath.append("\\kernelfile.c");
	std::cout << "path:" <<currentpath << std::endl;

    fumeCL fuCL= fumeCL();
    bundle = fuCL.getBundleInheap(bestGPU);
	cvimg = new Mat();
    pressed =0;


	Fl::add_timeout(1.0, Timer_CB, (void*)this);

}


Fl_cvDisplay::~Fl_cvDisplay()
{

}

void Fl_cvDisplay::readimg(const char * str){
    string t(str);
    imagepath = t;
	*cvimg = imread(str);
}

void Fl_cvDisplay::show_origin(){
    cvtColor(*cvimg, *cvimg, CV_BGR2RGB);

   // fumeAlg().gaussianFilter(1,0.2);
}

void Fl_cvDisplay::opencv_grayscale(){
    cvtColor(*cvimg,*cvimg,CV_BGR2GRAY);
}



void Fl_cvDisplay::grayscale(){

}

void Fl_cvDisplay::cycleChannel0(){

}

void Fl_cvDisplay::cycleChannel1(){

}

void Fl_cvDisplay::cycleChannel2(){

}

void Fl_cvDisplay::opencv_grayscale_smooth(){
    //cvtColor(*cvimg,*cvimg,CV_BGR2GRAY);
    int r = this->filterR;
    r = 2*r+1;
    float s = this->sigma;
    GaussianBlur(*cvimg, *cvimg, Size(r,r),sigma,sigma);
}

void Fl_cvDisplay::grayscale_smooth(){
    cvtColor(*cvimg, *cvimg, CV_BGR2RGBA);
    cvimg->convertTo(*cvimg,CV_32FC4);
    int r = this->filterR;
    float s = this->sigma;
    fumeFilter f = fumeAlg().gaussianFilter(r,s);

    cl::Kernel kernel = bundle->compileKernel(currentpath,"convoluteImage");
    cl::Image2D imginput = bundle->LoadImage(cvimg->cols,cvimg->rows,&cvimg->at<cv::Vec4f>(0,0));
    cl::Image2D imgoutput= bundle->createOutputImageFor(imginput);
    cl::Sampler default_sampler = bundle->createSampler();
    cl::Buffer filterbuf = bundle->createBuffer(CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, (2*r+1)*(2*r+1)*sizeof(float), f.data);
    cl::Buffer radius = bundle->intBuffer(r);
    kernel.setArg(0, imginput);
    kernel.setArg(1, imgoutput);
    kernel.setArg(2, default_sampler);
    kernel.setArg(3, filterbuf);
    kernel.setArg(4, radius);
    long st = clock();
    bundle->run2D(kernel,cvimg->cols,cvimg->rows,32,32);
    long en = clock();
    cout << "run time :" << en-st << endl;
    float * data =new float[cvimg->rows*cvimg->cols*4];
    bundle->readImage(imgoutput,(void*) data);
    Mat res = Mat(cvimg->rows, cvimg->cols, CV_32FC4, data);
    *cvimg = res.clone();
    cvimg->convertTo(*cvimg,CV_8UC4);
    cvtColor(*cvimg, *cvimg, CV_RGBA2RGB);
    delete[] data;
}


void Fl_cvDisplay::opencv_grayscale_x(){
    Sobel(*cvimg, *cvimg, CV_32FC1, 1, 0, 3, BORDER_DEFAULT);
    normalize(*cvimg,*cvimg, 0,255,NORM_MINMAX, CV_8UC1);
}

void Fl_cvDisplay::opencv_grayscale_y(){
    Sobel(*cvimg, *cvimg, CV_32FC1, 0, 1, 3, BORDER_DEFAULT);
    normalize(*cvimg,*cvimg, 0,255,NORM_MINMAX, CV_8UC1);
}

void Fl_cvDisplay::opencv_grayscale_xy(){
    Mat dx,dy;
    Sobel(*cvimg, dx, CV_32FC1, 1, 0, 3, BORDER_DEFAULT);
    Sobel(*cvimg, dy, CV_32FC1, 0, 1, 3, BORDER_DEFAULT);
    pow(dx, 2.0, dx);
	pow(dy, 2.0, dy);
	add(dx,dy,dx);
    sqrt(dx,dx);
    normalize(dx,*cvimg, 0,255,NORM_MINMAX, CV_8UC1);
}

void Fl_cvDisplay::opencv_gradient_vector(){
    Mat dx,dy;
    int N = this->pixel;
    int K = 5;

    Sobel(*cvimg, dx, CV_32FC1, 1, 0, 3, BORDER_DEFAULT);
    Sobel(*cvimg, dy, CV_32FC1, 0, 1, 3, BORDER_DEFAULT);

    float * angle =new float[cvimg->cols*cvimg->rows];
    for(int w = 0 ;w<cvimg->rows ;w++){
            for(int h=0;h<cvimg->cols;h++){
                angle[h*(cvimg->rows)+w] =
                atan2(dy.at<float>(w,h),dx.at<float>(w,h));
            }
    }

    for(int w = 0 ;w<cvimg->rows ;w+=N){
            for(int h=0;h<cvimg->cols;h+=N){
                    Point pt1=Point(h,w);
                    Point pt2=Point(cos(angle[h*(cvimg->rows)+w])*K+h,sin(angle[h*(cvimg->rows)+w])*K+w);
                    line(*cvimg,pt1,pt2,Scalar(0,255,0));
            }
    }
    delete[] angle;
}

void Fl_cvDisplay::opencv_rotate(){
    float a = this->degree;
    int len = std::max(cvimg->cols, cvimg->rows);
    Point2f pt(len/2,len/2);
    Mat r = getRotationMatrix2D(pt,a,1.0);
    warpAffine(*cvimg, *cvimg, r, Size(len, len));
}

void Fl_cvDisplay::draw(){
    long starttime = clock();
    draw2();
    long endtime = clock();
    cout << "frame time =" << endtime - starttime << endl;


	if (!cvimg->empty()){
		if (cvimg->channels() == 3){
			fl_draw_image(cvimg->datastart, this->x(), this->y(), cvimg->cols, cvimg->rows, 3, 0);
		}
		if (cvimg->channels() == 1){
			fl_draw_image_mono(cvimg->datastart, this->x(), this->y(), cvimg->cols, cvimg->rows);
		}
	}
}

void Fl_cvDisplay::draw2(){
    if(!cap.isOpened()){
        cap = VideoCapture(0);
    }
    filterR = ((mainWindow*)this->parent())->getgaussian();
    sigma   = ((mainWindow*)this->parent())->getcorr();
    pixel   = ((mainWindow*)this->parent())->getweight();
    degree  = ((mainWindow*)this->parent())->getthres();
    if(modeG==1){
        Mat frame;
        cap >> frame;
        *cvimg = frame.clone();
    }
    else if(modeG ==2){
        *cvimg = imread(imagepath);
    }
    switch(pressed){
            case 'i':
                show_origin();
                break;
            case 0:
                show_origin();
                break;
            case 'g':
                opencv_grayscale();
                break;
            case 'G':
                grayscale();
                break;
            case 'c':
                cycleChannel0();
                break;
            case 1:
                cycleChannel1();
                break;
            case 2:
                cycleChannel2();
                break;
            case 's':
                opencv_grayscale_smooth();
                break;
            case 'S':
                grayscale_smooth();
                break;
            case 'x':
                opencv_grayscale_x();
                break;
            case 'y':
                opencv_grayscale_y();
                break;
            case 'm':
                opencv_grayscale_xy();
                break;
            case 'p':
                opencv_gradient_vector();
                break;
            case 'r':
                opencv_rotate();
                break;
            case 'w':
                saveimg();
                pressed = 0;
                break;
            default:
                show_origin();
        }
        backup = cvimg->clone();
}

void Fl_cvDisplay::resize(int X,int Y,int W,int H ){

}
