#include "mainWindow.h"
using namespace cv;
using namespace std;
mainWindow::mainWindow(int w, int h ,const char * title):keyEventWindow(w, h, title)
{
	//cout << "mainwindow add:" << this << endl;
	begin();
	gaussian = new Fl_Slider(0,25,800,20,"Filter Size(Radius)");
	gaussian->type(1);
	gaussian->bounds(1,20);
	gaussian->value(1);
	correlation = new Fl_Slider(800, 25, 800, 20, "Sigma");
	correlation->type(1);
	correlation->bounds(0.01, 2);
	correlation->value(1);
	traceW = new Fl_Slider(0, 70, 800, 20, "pixel gap size");
	traceW->type(1);
	traceW->bounds(4, 100);
	traceW->value(10);
	threshold = new Fl_Slider(800, 70, 800, 20, "Degree");
	threshold->type(1);
	threshold->bounds(0, 360);
	threshold->value(30);
		menu =new Fl_Menu_Bar(0, 0, this->w(), 25);
		menu->add("&File/&Open", 0, open);
		menu->add("&File/&Camera", 0, cam);
		menu->add("&Help/&Description", 0, help);
        box = new Fl_cvDisplay( 0, 25+100, 800, 600, "");
        box->show();
	end();
	Fl::focus(this);
	cache =0;
	this->show();
}

double mainWindow::getgaussian(){
	return gaussian->value();
}
double mainWindow::getcorr(){
	return correlation->value();
}
double mainWindow::getweight(){
	return traceW->value();
}
double mainWindow::getthres(){
	return threshold->value();
}

mainWindow::~mainWindow()
{
}


void mainWindow::read(const char * str){
	box->readimg(str);
}

void mainWindow::opencb(Fl_File_Chooser *w, void *userdata)
{
	if (!w->visible()){
		std::cout << "File selected:" << w->value() << std::endl;
		mainWindow * winNow =  (mainWindow *)userdata;
		winNow->box->readimg(w->value());
        winNow->box->modeG=2;
	}
}

void mainWindow::opencb2(Fl_File_Chooser *w, void *userdata)
{
	if (!w->visible()){
		std::cout << "File selected:" << w->value() << std::endl;
		mainWindow * winNow = (mainWindow *)userdata;
		winNow->box2->readimg(w->value());

	}
}

void mainWindow::open(Fl_Widget* widget, void * data){
	Fl_File_Chooser *fc = new Fl_File_Chooser(".", "Image Files (*.{bmp,gif,jpg,png})", 1, "chooser an image");
	fc->callback(opencb, widget->parent());
	fc->show();
}

void mainWindow::cam(Fl_Widget* widget, void * data){
    mainWindow * xw =(mainWindow*) widget->parent();
    xw->box->modeG = 1;

}

void mainWindow::slidercb(Fl_Widget* widget, void * data){
    mainWindow * wx =(mainWindow*)data;
    //((Fl_cvDisplay*)wx->box)->redraw();

}

void mainWindow::help(Fl_Widget* widget, void * data){
    std::string msg2  ("'i' - reload the original image (i.e. cancel any previous processing)\n");
    std::string msg3  ("'w' - save the current (possibly processed) image into the file 'out.jpg'\n");
    std::string msg4  ("'g' - convert the image to grayscale using the openCV conversion function.");
    std::string msg5  ("'G' - convert the image to grayscale using your implementation of conversion function.\n");
    std::string msg6  ("'c' - cycle through the color channels of the image showing a different channel every time the key is pressed.\n");
    std::string msg7  ("'s' - convert the image to grayscale and smooth it using the openCV function. Use a track bar to control the amount of smoothing.\n");
    std::string msg8  ("'S' - convert the image to grayscale and smooth it using your function which should perform convolution with a suitable filter. Use a track bar to control the amount of smoothing.\n");
    std::string msg9  ("'x' - convert the image to grayscale and perform convolution with an x derivative filter. Normalize the obtained values to the range [0,255].\n");
    std::string msg10 ("'y' - convert the image to grayscale and perform convolution with a y derivative filter. Normalize the obtained values to the range [0,255].\n");
    std::string msg11 ("'m' - show the magnitude of the gradient normalized to the range [0,255]. The gradient is computed based on the x and y derivatives of the image.\n");
    std::string msg12 ("'p' - convert the image to grayscale and plot the gradient vectors of the image every N pixels and let the plotted gradient vectors have a length of K. Use a track bar to control N. Plot the vectors as short line segments of length K.\n");
    std::string msg13 ("'r' - convert the image to grayscale and rotate it using an angle of Q degrees. Use a track bar to control the rotation angle. The rotation of the image should be performed using an inverse map so there are no holes in it.\n");
    std::string msg14 ("'h' - Display a short description of the program, its command line arguments, and the keys it supports.");
    std::string msg = msg2+msg3+msg4+msg5+msg6+msg7+msg8+msg9+msg10+msg11+msg12+msg13+msg14;
    std::cout << msg << std::endl;
    fl_alert(msg.c_str());
}

void
mainWindow::draw(){
    Fl_Window::draw();
}



int mainWindow::handle_key(int e, int key){
    static int cachekey = 0;
	if ('a' <= key&&key <= 'z'&&Fl::event_shift()){
		char c = key - 32;
		std::cout << c << " pressed " << std::endl;
		if (c == 'G'){
			box->pressed = 'G';
		}
		if (c == 'S'){
			box->pressed = 'S';
		}
	}
	else
	{
		char c = key;
		if (c == 'i'){
			box->pressed = 'i';
		}
        if (c == 'w'){
			box->pressed = 'w';
		}
        if (c == 'g'){
			box->pressed = 'g';
		}
        if (c == 'c'){
            if(cachekey ==0){
                box->pressed = 'c';
                cachekey=1;
            }
            else if(cachekey ==1){
                box->pressed = 1;
                cachekey =2;
            }
            else if(cachekey ==2){
                box->pressed = 2;
                cachekey =0;
            }
		}
		if (c == 's'){
			box->pressed = 's';
		}
        if (c == 'x'){
			box->pressed = 'x';
		}
        if (c == 'y'){
			box->pressed = 'y';
		}
        if (c == 'm'){
			box->pressed = 'm';
		}
        if (c == 'p'){
			box->pressed = 'p';
		}
        if (c == 'r'){
			box->pressed = 'r';
		}
		if (c == 'h'){
			box->pressed = 'h';
		}
		std::cout << c << std::endl;
	}
	return 1;
}
