#ifndef LOOPWINDOW_H
#define LOOPWINDOW_H

#include "keyEventWindow.h"
#include <pthread.h>
#include <semaphore.h>
#include "wincommon.h"

class loopWindow:
    public keyEventWindow
{
    public:

        pthread_t thread;
        int thread_exit_flag;
        int freshRate;
        loopWindow(int, int, const char *);
        virtual ~loopWindow();

        void setFreshRate(int);
        int getFreshRate();
        virtual void draw();


    protected:

    private:
        static void Timer_CB(void * userdata);   // refresh the widget
        static void * Thread_CB(void* userdata);    // pthread call back function
};

#endif // LOOPWINDOW_H
