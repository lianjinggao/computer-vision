#ifndef FUMECLCOMMON_H_INCLUDED
#define FUMECLCOMMON_H_INCLUDED

#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <utility>
#include <cstddef>
#include <vector>

#if defined(__APPLE__) || defined(__MACOSX)
#include "cl.hpp"
#include <OpenGL/OpenGL.h>
#include <OpenCL/opencl.h>
#include <libkern/OSAtomic.h>
#else
#include <CL/cl.h>
#include <cl.hpp>
#endif



#endif // FUMECLCOMMON_H_INCLUDED
