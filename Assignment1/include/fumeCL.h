#pragma once
#include "fumeCLcommon.h"
#include "fumeCLBundle.h"
class fumeCL
{

public:
    std::vector<cl::Platform> platforms;
	size_t platFormNumber;
	cl::Context defaultContext;
	cl::Device  defaultDevice;
	cl::Program defaultProgram;
	cl::Kernel  defaultKernel;
	std::vector<fumeCLBundle> pool;
public:
	fumeCL();
	~fumeCL();
	void platformInfo(void);
	void setDefaults(std::string kernelPath);
	fumeCLBundle getBundle(bundleType);// used the device with most group size as the approx best device
	fumeCLBundle* getBundleInheap(bundleType);
	fumeCLBundle fumeCLBenckMark(); // Have to do benchmark and pick the best performance device!

private:
	void dumpPlatform(int type, cl::Platform platform, bool brief = false);
	void dumpDevice(cl::Device device, bool brief = false);
	void dumpFPConfig(cl_device_fp_config bitfield);

};


