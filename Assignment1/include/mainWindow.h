#pragma once
#include "keyEventWindow.h"
#include "wincommon.h"
#include "Fl_cvDisplay.h"

class mainWindow :
	public keyEventWindow
{
public:
	mainWindow(int, int, const char *);
	~mainWindow();
	double getgaussian();
	double getcorr();
	double getweight();
	double getthres();
	int handle_key(int e, int key);
	void draw();
	int cache;
private:
	Fl_Menu_Bar * menu;
	Fl_cvDisplay * box;
	Fl_cvDisplay * box2;

	Fl_Slider    *gaussian;
	Fl_Slider    *correlation;
	Fl_Slider    *traceW;
	Fl_Slider    *threshold;

	static void open(Fl_Widget* widget, void * data);
	static void opencb(Fl_File_Chooser *w, void *userdata);
	static void opencb2(Fl_File_Chooser *w, void *userdata);
	static void cam (Fl_Widget* widget, void * data);
	static void help(Fl_Widget* widget, void * data);
	static void slidercb(Fl_Widget*, void*);

	void read(const char *);
};

