#ifndef FUMECLBUNDLE_H_INCLUDED
#define FUMECLBUNDLE_H_INCLUDED
#include "fumeCLcommon.h"


const char * get_error_string(cl_int err);
inline void fumeCheckError(cl_int err, const char * name);
enum bundleType{bestGPU, bestCPU };

class fumeCLBundle
{
public:
	cl::Context context;
	std::vector<cl::Device> devices;
	cl::Device device;
	cl::Program program;
	//cl::Kernel kernel;
	cl::CommandQueue queue;
	cl::Event event;
    std::string previousfunc;

private:
    //int arg_index;

public:
    
    fumeCLBundle(){};
	fumeCLBundle(cl::Context c, std::vector<cl::Device> d);
	fumeCLBundle(cl::Context c, cl::Device dev);
	~fumeCLBundle();

    // kernel compile functions
	cl::Kernel compileKernel(std::string path, std::string func);

    // run kernel functions
	void run1D(cl::Kernel &kernel, size_t length);
	void run2D(cl::Kernel &kernel, size_t global_x, size_t global_y, size_t local_x, size_t local_y);

    // Buffer prepare functions
	cl::Image2D LoadImage(std::string imagePath);
	cl::Image2D LoadImage(int w,int h, void * buf);
	cl::Buffer  createBuffer(cl_mem_flags flags, size_t length, void* buffer);
	cl::Buffer  intBuffer(int number);

	cl::Image2D createOutputImageFor(cl::Image2D &image);
	cl::Sampler createSampler();

    // Result getting functions
	void getBuffer(int i, void * buf, size_t length);
	void readImage(cl::Image2D &image, void * buffer);

};

#endif // FUMECLBUNDLE_H_INCLUDED
