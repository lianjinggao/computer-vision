#ifndef FEATUREDETECT_H
#define FEATUREDETECT_H

#include "wincommon.h"
using namespace cv;
class featureDetect
{
    public:
        featureDetect();
        virtual ~featureDetect();
        static Mat localMax(Mat input, int step, int thresh );

    protected:
    private:
};

#endif // FEATUREDETECT_H
