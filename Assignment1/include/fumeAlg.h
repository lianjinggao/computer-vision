#ifndef FUMEALG_H
#define FUMEALG_H

#include "fumeCL.h"

class fumeFilter{
    public:
        int r;
        int w;
        int h;
        void * data;
        int unitSize;
        int totalSize;
    public:
        fumeFilter(int radius, int width, int height, void* buffer, int unitsize):r(radius),w(width),h(height),data(buffer),unitSize(unitsize){totalSize = width*height;};
        virtual ~fumeFilter();
};



class fumeAlg
{
    public:
        fumeAlg();
        virtual ~fumeAlg();
        // all filter is normalized
        fumeFilter gaussianFilter(int radius,float sigma);
        fumeFilter simpleFilter(int radius);

    public://membersL:
        fumeCLBundle * bundle;
        float pi;

    protected:
    private:
};

#endif // FUMEALG_H
