#pragma once
#include "wincommon.h"
using namespace cv;
class Fl_cvDisplay :
	public Fl_Box
{
public:
	Mat * cvimg;
	//Mat * save;
	Mat backup;
	int pressed;
	fumeCLBundle * bundle;
    VideoCapture cap;
    //args
	int filterR;
	float sigma;
	int pixel;
    float degree;
    int modeG;
	Fl_cvDisplay( int, int, int, int, const char*);
	~Fl_cvDisplay();

	std::string currentpath;
    std::string imagepath;

    static void Timer_CB(void * userdata);

	void readimg(const char*);
	void draw();
	void draw2();
	void resize(int X,int Y,int W,int H );
	void show_origin();//i
	void opencv_grayscale();//g
    void grayscale();//G
    void cycleChannel0();//c
    void cycleChannel1();//c
    void cycleChannel2();//c
    void opencv_grayscale_smooth();//s
    void grayscale_smooth();//S
    void opencv_grayscale_x();//x
    void opencv_grayscale_y();//y
    void opencv_grayscale_xy();//m
    void opencv_gradient_vector();//p
    void opencv_rotate();//r
    void saveimg();
};

