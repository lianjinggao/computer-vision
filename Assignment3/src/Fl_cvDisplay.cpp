#include "Fl_cvDisplay.h"
#include "mainWindow.h"
#include <iostream>
#include <fstream>

using namespace std;

void Fl_cvDisplay::Timer_CB(void* userdata){
    Fl_cvDisplay *o = (Fl_cvDisplay*)userdata;
    o->redraw();
    Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
}

Fl_cvDisplay::Fl_cvDisplay(int x, int y, int w, int h,const char* l) :Fl_Box(x,y,w,h,l)
{
    modeG =2;
	cvimg = new Mat();
    pressed =0;
	Fl::add_timeout(1.0, Timer_CB, (void*)this);
}

Fl_cvDisplay::~Fl_cvDisplay()
{

}

void Fl_cvDisplay::readimg(const char * str){
    string t(str);
    imagepath = t;
	Mat temp = imread(str);
	cvtColor(temp, *cvimg, CV_BGR2GRAY);


}

void Fl_cvDisplay::draw(){
    Mat output = cvimg->clone();
    findChessBoard(*cvimg , output);
    if(pressed == 's'){
        ofstream myfile ("example.txt");
        if (myfile.is_open())
        {
            myfile << corners.size() << '\n';
            for(int i = 0; i< corners.size();i++){
                myfile << corners[i].x <<" "<< corners[i].y <<'\n';

            }
            myfile.close();
        }
        pressed = 0;
    }

	if (!output.empty()){
        if(output.cols!=800||output.rows!=600){
            cv::resize(output,output,Size(800,600));
        }
		if (output.channels() == 3){
			fl_draw_image(output.datastart, this->x(), this->y(), output.cols, output.rows, 3, 0);
		}
		if (output.channels() == 1){
			fl_draw_image_mono(output.datastart, this->x(), this->y(), output.cols, output.rows);
		}
	}
}

void Fl_cvDisplay::findChessBoard(Mat & imgsrc, Mat & imgout ){

    mainWindow* o = (mainWindow*)(this->parent());
    int y = o->getrows();
    int x = o->getcols();
    if(!imgsrc.empty()){
        Size patternsize(y,x);
        bool patternfound = findChessboardCorners(imgsrc, patternsize, corners,CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
        if(patternfound){
            cornerSubPix(imgsrc, corners, Size(20, 20), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
            drawChessboardCorners(imgout , patternsize, Mat(corners), patternfound);
            cout << "Chess-board found!" << corners.size() <<endl;
        }
    }
}

void Fl_cvDisplay::resize(int X,int Y,int W,int H ){

}
