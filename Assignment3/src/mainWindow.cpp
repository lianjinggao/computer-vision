#include "mainWindow.h"
using namespace cv;
using namespace std;
mainWindow::mainWindow(int w, int h ,const char * title):keyEventWindow(w, h, title)
{
	//cout << "mainwindow add:" << this << endl;
	begin();
	yrows = new MyValueSlider(0,25,800,20,"rows");
	yrows->type(1);
	yrows->bounds(1,20);
	yrows->value(9);
	xcols = new MyValueSlider(800, 25, 800, 20, "cols");
	xcols->type(1);
	xcols->bounds(1, 20);
	xcols->value(6);
		menu =new Fl_Menu_Bar(0, 0, this->w(), 25);
		menu->add("&File/&Open", 0, open);
		//menu->add("&File/&Camera", 0, cam);
		menu->add("&Help/&Description", 0, help);
        box = new Fl_cvDisplay( 0, 25+100, 800, 600, "");
        box->show();
	end();
	Fl::focus(this);
	cache =0;
	this->show();
}

double mainWindow::getrows(){
	return yrows->value();
}
double mainWindow::getcols(){
	return xcols->value();
}

mainWindow::~mainWindow()
{
}


void mainWindow::read(const char * str){
	box->readimg(str);
}

void mainWindow::opencb(Fl_File_Chooser *w, void *userdata)
{
	if (!w->visible()){
		std::cout << "File selected:" << w->value() << std::endl;
		mainWindow * winNow =  (mainWindow *)userdata;
		winNow->box->readimg(w->value());
        winNow->box->modeG=2;
	}
}

void mainWindow::opencb2(Fl_File_Chooser *w, void *userdata)
{
	if (!w->visible()){
		std::cout << "File selected:" << w->value() << std::endl;
		mainWindow * winNow = (mainWindow *)userdata;
		winNow->box2->readimg(w->value());

	}
}

void mainWindow::open(Fl_Widget* widget, void * data){
	Fl_File_Chooser *fc = new Fl_File_Chooser(".", "Image Files (*.{bmp,gif,jpg,png})", 1, "chooser an image");
	fc->callback(opencb, widget->parent());
	fc->show();
}

void mainWindow::cam(Fl_Widget* widget, void * data){
    mainWindow * xw =(mainWindow*) widget->parent();
    xw->box->modeG = 1;

}

void mainWindow::slidercb(Fl_Widget* widget, void * data){
    mainWindow * wx =(mainWindow*)data;
}

void mainWindow::help(Fl_Widget* widget, void * data){

    std::string msg("press 's': Save data to output.txt");
    std::cout << msg << std::endl;
    fl_alert(msg.c_str());
}

void
mainWindow::draw(){
    Fl_Window::draw();
}



int mainWindow::handle_key(int e, int key){
    static int cachekey = 0;
	if ('a' <= key&&key <= 'z'&&Fl::event_shift()){
		char c = key - 32;
		std::cout << c << " pressed " << std::endl;
		if (c == 'G'){
			box->pressed = 'G';
		}
		if (c == 'S'){
			box->pressed = 'S';
		}
	}
	else
	{
		char c = key;
		if (c == 's'){
			box->pressed = 's';
		}
		std::cout << c << std::endl;
	}
	return 1;
}
