#pragma once
#include "wincommon.h"
using namespace cv;
class Fl_cvDisplay :
	public Fl_Box
{
public:
	Mat * cvimg;

	Mat backup;
	int pressed;
	//fumeCLBundle * bundle;
    VideoCapture cap;
    vector<Point2f> corners;
    //args
	int filterR;
	float sigma;
	int pixel;
    float degree;
    int modeG;
	Fl_cvDisplay( int, int, int, int, const char*);
	~Fl_cvDisplay();

	std::string currentpath;
    std::string imagepath;

    static void Timer_CB(void * userdata);

	void readimg(const char*);
	void draw();
	void draw2();
	void findChessBoard(Mat & imgsrc, Mat & imgout );
	void resize(int X,int Y,int W,int H );

};

