#ifndef WINCOMMON_H_INCLUDED
#define WINCOMMON_H_INCLUDED
//(* These headers are for standard C++
#include <direct.h>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <utility>
//*) These headers are for standard C++

//(* These headers are for standard FLTK widgets
#if defined(__MINGW32__)||defined(__CYGWIN__)
#else
#define WIN32 // This is for FLTK in windows
#endif // defined

#include <FL/Fl.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Slider.H>
#include <FL/fl_ask.H>

//*) These headers are for standard FLTK widgets

//(* These files are for Pthread
//#include <pthread.h>
//*) These files are for Pthread

//(* These headers are for opencv
#include <opencv2/opencv.hpp>
//*) These headers are for opencv




//(* These headers are for freeImage
//*) These headers are for freeImage

#endif // WINCOMMON_H_INCLUDED
