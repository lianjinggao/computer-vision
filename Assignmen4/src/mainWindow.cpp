#include "mainWindow.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include "ImageWinCom.h"
using namespace std;

vector<mat> alg3(vector<mat> &p1, vector<mat> &p2);
void SVD_refine(mat &M);
double points_normalize(vector<mat> &points);
vector<mat> epipolesF(mat &F);
void SVD_refine(mat &M){
    if(!M.is_square()){
        throw "This is not a square Matrix";
        return;
    }
    mat V,U;
    vec s;
    svd(U,s,V,M);
    M.clear();
    // Find samllest value in vec s
    double smallest;
    smallest = s[0];
    unsigned int index = 0;
    for (unsigned int i = 0; i< s.size() ; i++) {
        if(smallest > s[i]){
            smallest = s[i];
            index = i;
        }
    }
    s[index] = 0.0;
    M = U*diagmat(s)*V.t();
}

mainWindow::mainWindow(int w, int h ,const char * title):Fl_Window(w, h, title){
	begin();
		menu =new Fl_Menu_Bar(0, 0, this->w(), 25);
		menu->add("&Help/&Description", 0, help);
        runalg = new Fl_Button(50,350,500,40,"Generate Fundamental Matrix");
        runalg -> callback(algrun,(void*)this);
        com3 =new  FileChooserCom(50,50,500,40,"Open Left Image");
        com4 =new  FileChooserCom(50,150,500,40,"Open Right Image");
        //string temp3("D:\\downloads\\J1-l.tif");
        //string temp4("D:\\downloads\\J1-r.tif");
        //com3->value = temp3;
        //com4->value = temp4;
	end();
    com1 = new ImageWinCom("left image");
    com2 = new ImageWinCom("right image");
	Fl::focus(this);
	cache =0;
	this->show();
    Fl::add_timeout(1.0, Timer_CB, (void*)this);

}

void mainWindow::Timer_CB(void* userdata){
    mainWindow *o = (mainWindow*)userdata;
    if(!o->com3->value.empty()){
        string temp1 = o->com3->value;
        o->com1->read(temp1);
        o->com3->value.clear();
    }
    if(!o->com4->value.empty()){
        string temp2 = o->com4->value;
        o->com2->read(temp2);
        o->com4->value.clear();
    }
    if(!o->F.empty()){
        o->drawlines();
    }
    Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
}

mainWindow::~mainWindow()
{

}

//com1 is left image com2 is right image
void mainWindow::drawlines(){
    if(com1->points.size()>com2->lines.size()){
        mat temp = com1->points[com1->points.size()-1];
        vec3 x; x<< temp(0,0) <<temp(0,1) <<1;
        vec3 A =F * x;
        com2->lines.push_back(A);
    }
    if(com2->points.size()>com1->lines.size()){
        mat temp = com2->points[com2->points.size()-1];
        vec3 x; x<< temp(0,0) <<temp(0,1) <<1;
        vec3 A =F * x;
        com1->lines.push_back(A);
    }
}


void mainWindow::help(Fl_Widget* widget, void * data){
    std::string msg("please read two points files and press run to get result in command line");
    std::cout << msg << std::endl;
    fl_alert(msg.c_str());
}

void mainWindow::algrun(Fl_Widget *w, void *data){
    ((mainWindow*)data)->algrun2();
}

void mainWindow::algrun2(){
    vector<mat> res = alg3(com2->points,com1->points);
    el = res[0];
    er = res[1];
    F  = res[2];
    cout << "left epipole" << "[" <<el(0,0)/el(0,2)<<" "<<el(0,1)/el(0,2) <<"]" <<endl;
    cout << "right epipole"<< "[" <<er(0,0)/er(0,2)<<" "<<er(0,1)/er(0,2) <<"]" <<endl;
    cout << "fundamental Matrix" << endl; cout << F <<endl;
    com1->points.clear();
    com2->points.clear();
}

//pt2 is left pt1 is right
vector<mat> alg3(vector<mat> &pt1, vector<mat> &pt2){
    vector<mat> res;
    if(pt1.size()!=pt2.size()||pt1.size()<8){
        throw "error";
        return res;
    }
    mat bigA;

    for(size_t i = 0; i< pt1.size();i++){
        mat p1 = pt1[i];
        mat p2 = pt2[i];
        mat m19;
        m19 << p1(0,0)*p2(0,0) << p1(0,0)*p2(0,1) << p1(0,0) <<  p1(0,1)*p2(0,0) << p1(0,1)*p2(0,1) << p1(0,1) <<p2(0,0) << p2(0,1) <<1 <<endr;
        bigA = join_vert(bigA, m19);
    }
    mat V,U;
    vec s;
    svd( U, s, V,bigA);
    mat bigF(3,3);
    bigF<< V(0,8) << V(1,8) <<V(2,8)<<endr
        << V(3,8) << V(4,8) <<V(5,8)<<endr
        << V(6,8) << V(7,8) <<V(8,8)<<endr;
    SVD_refine(bigF);
    res = epipolesF(bigF);
    res.push_back(bigF);
    return res;
}

double points_normalize(vector<mat> &points){

    double u=0;
    double v=0;
    for (unsigned int  t = 0; t< points.size() ; t++) {
        u += points[t][0];
        v += points[t][1];
    }
    u /= points.size();
    v /= points.size();
    double sigma = 0;
    for (unsigned int  t = 0; t< points.size() ; t++) {
        sigma+= pow(points[t][0] - u,2);
        sigma+= pow(points[t][1] - v,2);
    }
    sigma /= points.size();
    sigma = sqrt(sigma);
    return sigma;
}

vector<mat> epipolesF(mat &F){
    vector<mat> result;
    if(F.size()==9&&F.is_square()){
        mat V,U;
        vec s;
        svd(U,s,V,F);
        mat el(1,3);mat er(1,3);
        el<< V(0,2) << V(1,2) << V(2,2);
        cout << "left epipole is " << el<<endl;
        s.clear();
        V.clear();
        U.clear();
        svd(U,s,V,F.t());
        er<< V(0,2) << V(1,2) << V(2,2);
        cout << "right epipole is " << er<<endl;
        result.push_back(el);
        result.push_back(er);
        return result;
    }
    throw "This is not a square Matrix";
}
