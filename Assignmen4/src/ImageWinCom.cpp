//
//  ImageWinCom.cpp
//  testArmadillo
//
//  Created by LianjingGao on 4/17/14.
//  Copyright (c) 2014 LianjingGao. All rights reserved.
//

#include "ImageWinCom.h"
#include <FL/fl_draw.H>

ImageWinCom::ImageWinCom(char * t):keyEventWindow(1,1,t){
    Fl::add_timeout(1.0, Timer_CB, (void*)this);
}

ImageWinCom::ImageWinCom(string path):keyEventWindow(1,1,""){
    img = imread(path.c_str());
    this->resize(this->x(), this->y(), img.cols, img.rows);
    show();
    Fl::add_timeout(1.0, Timer_CB, (void*)this);
}

void ImageWinCom::read(string path){
    img = imread(path.c_str());
    this->resize(this->x(), this->y(), img.cols, img.rows);
    show();
}

void ImageWinCom::Timer_CB(void* userdata){
    ImageWinCom *o = (ImageWinCom*)userdata;
    o->redraw();
    Fl::repeat_timeout(1.0/24.0, Timer_CB, userdata);
}

void ImageWinCom::draw(){
    if(img.empty()) return Fl_Window::draw();
    cv::Mat out = img.clone();
    for(unsigned int i = 0 ;i<points.size() ; i++){
        arma::mat temp = points[i];
        circle(out, Point(temp(0,0),temp(0,1)), 3, Scalar(255,255,0));
    }
    for(unsigned int i=0 ;i<lines.size();i++){
        double x1 = (double)img.cols*(-1);
        double y1 = (lines[i](2)+(x1*lines[i](0)))/lines[i](1)*(-1);

        double x2 = (double)img.cols*(2);
        double y2 = (lines[i](2)+(x2*lines[i](0)))/lines[i](1)*(-1);
        line(out, Point((int)x1,(int)y1), Point((int)x2,(int)y2), Scalar(255,255,0),3);
    }
    if(img.channels()==1){
        fl_draw_image_mono(out.data, this->x(), this->y(), img.cols, img.rows);
    }
    if(img.channels()==3){
        fl_draw_image(out.data, 0, 0, img.cols, img.rows,3,0);
    }
}

int ImageWinCom::ImageWinCom::handle_mouse(int e, int key){
    if(e==Fl::event_button()&&key == FL_LEFT_MOUSE){
        arma::mat point;
        point << (double)Fl::event_x() << (double)Fl::event_y() << 1 << arma::endr;
        points.push_back(point);
        cout << points.size()<<endl;
        return 1;
    }
    else if(key == FL_RIGHT_MOUSE&&e==FL_RELEASE){
        points.clear();
        cout << points.size()<<endl;
    }
    return 1;
}
