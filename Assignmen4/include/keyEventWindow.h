#pragma once
#include "wincommon.h"

class keyEventWindow : public Fl_Window{
private:
	virtual int handle_key(int, int);
	virtual int handle_mouse(int, int);
public:
	keyEventWindow(int, int, const char *);
	~keyEventWindow();
	int handle(int e);
};
