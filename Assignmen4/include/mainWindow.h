#pragma once
#include "keyEventWindow.h"
#include "wincommon.h"
#include "mywidgets.hpp"
#include <armadillo>
#include "ImageWinCom.h"

using namespace arma;
class mainWindow :
	public Fl_Window
{
public:
	mainWindow(int, int, const char *);
	~mainWindow();
	int cache;

	mat el;
	mat er;
	mat F;
	void drawlines();
private:
	Fl_Menu_Bar * menu;
    Fl_Button * runalg;
    ImageWinCom * com1;
    ImageWinCom * com2;
    FileChooserCom * com3;
    FileChooserCom * com4;

	static void help(Fl_Widget* widget, void * data);
	static void algrun(Fl_Widget *w, void *data);
    void algrun2();
    static void Timer_CB(void* userdata);
};

