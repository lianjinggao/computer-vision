//
//  ImageWinCom.h
//  testArmadillo
//
//  Created by LianjingGao on 4/17/14.
//  Copyright (c) 2014 LianjingGao. All rights reserved.
//

#pragma once
#include <iostream>
#include "wincommon.h"
#include "keyEventWindow.h"
#include <opencv2/opencv.hpp>
#include <armadillo>
using namespace std;
using namespace cv;
class ImageWinCom: public keyEventWindow{
protected:
    cv::Mat img;
    int handle_mouse(int e, int key);
public:
    ImageWinCom(char* t);
    ImageWinCom(string path);
    void read(string path);
    void settitle(string t);
    virtual ~ImageWinCom(){};
    void draw();
    vector<arma::mat> points;
    vector<arma::vec3> lines;
    static void Timer_CB(void* userdata);

};
