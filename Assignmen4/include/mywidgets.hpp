#ifndef MYWIDGETS_HPP_INCLUDED
#define MYWIDGETS_HPP_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <FL/fl_draw.H>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Menu_Window.H>
#include <FL/Fl_Tooltip.H>

using namespace std;

class TipWin : public Fl_Menu_Window {
    char tip[40];
public:
    TipWin():Fl_Menu_Window(1,1) {      // will autosize
        strcpy(tip, "X.XX");
        set_override();
        end();
    }
    void draw() {
        draw_box(FL_BORDER_BOX, 0, 0, w(), h(), Fl_Color(175));
        fl_color(FL_BLACK);
        fl_font(labelfont(), labelsize());
        fl_draw(tip, 3, 3, w()-6, h()-6, Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_WRAP));
    }
    void value(float f) {
        sprintf(tip, "%.2f", f);
        // Recalc size of window
        fl_font(labelfont(), labelsize());
        int W = w(), H = h();
        fl_measure(tip, W, H, 0);
        W += 8;
        size(W, H);
        redraw();
    }
};

class MyValueSlider : public Fl_Slider {
    TipWin *tipwin;
    void value_cb2() {
        tipwin->value(value());
        tipwin->position(Fl::event_x_root(), Fl::event_y_root()+20);
    }
    static void value_cb(Fl_Widget*, void*data) {
        MyValueSlider *val = (MyValueSlider*)data;
        val->value_cb2();
    }
public:
    MyValueSlider(int x,int y,int w,int h,const char*l=0):Fl_Slider(x,y,w,h,l) {
        type(FL_HOR_SLIDER);
        callback(value_cb, (void*)this);
        Fl_Group *save = Fl_Group::current();   // save current widget..
        tipwin = new TipWin();                  // ..because this trashes it
        tipwin->hide();
        Fl_Group::current(save);                // ..then back to previous.
    }
    int handle(int e) {
        switch(e) {
            case FL_PUSH:
                // XXX: if offscreen, move tip ABOVE mouse instead
                tipwin->position(Fl::event_x_root(), Fl::event_y_root()+20);
                tipwin->value(value());
                tipwin->show();
                break;
            case FL_HIDE:       // valuator goes away
            case FL_RELEASE:    // release mouse
            case FL_LEAVE:      // leave focus
                // Make sure tipwin closes when app closes
                tipwin->hide();
                break;
        }
        return(Fl_Slider::handle(e));
    }
};


class FileChooserCom : Fl_Group{
protected:
Fl_Button * button;
Fl_Input * input;

void FCC_CB2() {
    Fl_File_Chooser chooser(".",                        // directory
                            "*",                        // filter
                            1,     // chooser type
                            "Choose a file");        // title
    chooser.show();
    while(chooser.shown())
        { Fl::wait(); }
    if ( chooser.value() == NULL )
        { fprintf(stderr, "(User hit 'Cancel')\n"); return; }
    fprintf(stderr, "--------------------\n");
    fprintf(stderr, "DIRECTORY: '%s'\n", chooser.directory());
    fprintf(stderr, "    VALUE: '%s'\n", chooser.value());
    fprintf(stderr, "    COUNT: %d files selected\n", chooser.count());

    input->value(chooser.value());
    string temp(chooser.value());
    value = temp;
    if ( chooser.count() > 1 ) {
        for ( int t=1; t<=chooser.count(); t++ ) {
            fprintf(stderr, " VALUE[%d]: '%s'\n", t, chooser.value(t));
        }
    }
}

static void FCC_CB(Fl_Widget *w, void *data) {
    ((FileChooserCom*)data)->FCC_CB2();
}

public:
    string value;
    FileChooserCom(int X,int Y,int W,int H,const char *L=0):Fl_Group(X,Y,W,H,L) {
        begin();
        button = new Fl_Button(X,Y,W/3,H,"Open");
        button->callback(FCC_CB,(void*)this);
        input = new Fl_Input(X+W/3,Y,W/3*2,H,"");
        end();
    }

    string getvalue(){
        return value;
    }

};

#endif // MYWIDGETS_HPP_INCLUDED
